﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SampleDB_02.Models;

namespace SampleDB_02.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly RestoDataContext _restoDataContext;

        private IEnumerable<Restorant> restorants;
        private int targetCityId;

        public HomeController(ILogger<HomeController> logger, RestoDataContext restoDataContext)
        {
            _logger = logger;
            _restoDataContext = restoDataContext;

            restorants = new List<Restorant>();
        }

        public IActionResult Index(int page = 1)
        {
            int pageSize = 3;
            restorants = _restoDataContext.Restorants.Where(x => x.OwnCity.Id == 1);//Каким-то образом нужно получать и сохранять между запросами ИД города
            ViewBag.PageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = restorants.Count() };

            IEnumerable<Restorant> restorantsPerPages = restorants.Skip((page - 1) * pageSize).Take(pageSize);

            return View(restorantsPerPages);
        }

        /// <summary>
        /// Получение списка ресторанов в указанном городе
        /// </summary>
        /// <param name="cityId">ИД города</param>
        /// <returns>Форма со списком ресторанов</returns>
        [HttpGet("RestInCity")]
        public async Task<IEnumerable<Restorant>> GetRestorantsInTargetCity([FromQuery] int cityId)
        {
            try
            {
                restorants = _restoDataContext.Restorants.Where(x=> x.OwnCity.Id == cityId);//тут падает из-за бесконечной вложенности. Нужно будет как-то исправить.
                return restorants;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        /// <summary>
        /// Добавление города
        /// </summary>
        /// <param name="cityName">Название нового добавляемого города</param>
        /// <returns></returns>
        [HttpPost("AddCity")]
        public async Task AddCity([FromBody] string cityName)
        {
            var city = new City() { Name = cityName };
            _restoDataContext.Citys.Add(city);
            await _restoDataContext.SaveChangesAsync();
        }

        /// <summary>
        /// Добавление нового ресторана к указанному городу.
        /// Если города нет в списке, он будет создан и добавлен в список. Новый ресторан будет размещен в новом городе.
        /// </summary>
        /// <param name="restoName">Название нового ресторана</param>
        /// <param name="cityName">Название города, куда будет добавлен ресторан</param>
        /// <returns></returns>
        /// <response code="200">Удалось добавить элемент</response>
        /// <response code="500">Что-то пошло не так</response>   
        [HttpPost("AddResto")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task AddRestorant(string restoName, string cityName)
        {
            try
            {
                var city = _restoDataContext.Citys.FirstOrDefault(x => x.Name.Equals(cityName));

                if (city == null)
                {
                    city = new City() { Name = cityName };
                    _restoDataContext.Citys.Add(city);
                    await _restoDataContext.SaveChangesAsync();
                }

                var resto = new Restorant() { Name = restoName, OwnCity = city };
                _restoDataContext.Restorants.Add(resto);
                await _restoDataContext.SaveChangesAsync();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
