﻿using System.Collections.Generic;

namespace SampleDB_02.Models
{
    public class IndexViewModel
    {
        public IEnumerable<Restorant> Restorants { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}
