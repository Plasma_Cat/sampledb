﻿using System.Collections.Generic;

namespace SampleDB_02.Models
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual HashSet<Restorant> Restorants { get; set; }
    }
}
