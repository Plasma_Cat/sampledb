﻿namespace SampleDB_02.Models
{
    public class Restorant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual City OwnCity { get; set; }
    }
}
