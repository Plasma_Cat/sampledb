﻿using System;
using Microsoft.EntityFrameworkCore;

namespace SampleDB_02.Models
{
    public class RestoDataContext : DbContext
    {
        public DbSet<City> Citys { get; set; }
        public DbSet<Restorant> Restorants { get; set; }

        public RestoDataContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var dbPath = System.IO.Path.Combine(Environment.CurrentDirectory, "RestoData.db");
            optionsBuilder.UseSqlite("Filename = " + dbPath);

        }
    }
}
