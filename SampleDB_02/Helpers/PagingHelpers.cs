﻿using System.Linq;
using System.Text;
using SampleDB_02.Models;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;

namespace SampleDB_02.Helpers
{
    public static class PagingHelpers
    {
        public static HtmlString Paging(this IHtmlHelper html, PageInfo pageInfo, IUrlHelper url)
        {
            StringBuilder sb = new StringBuilder();

            //foreach (var pageNum in Enumerable.Range(1, pageInfo.TotalPages))
            foreach (var pageNum in Enumerable.Range(1, 4))
            {
                if (pageNum == pageInfo.PageNumber)
                {
                    sb.Append($"<b> {pageNum}</b>");
                }
                else
                {
                    sb.Append($"<a href=\"{url.Action("Index", new { page = pageNum }) }\"> {pageNum}</a>");
                }
            }
            HtmlString htmlString = new HtmlString(sb.ToString());

            return htmlString;
        }
    }
}
